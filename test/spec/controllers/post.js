'use strict';

describe('Controller: PostCtrl', function () {

  // load the controller's module
  beforeEach(module('flickrPublicFeedApp'));

  var PostCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PostCtrl = $controller('PostCtrl', {
      $scope: scope
    });
  }));

  it('should all scope variables to be null on start', function () {
    expect(scope.title).toBe(null);
    expect(scope.link).toBe(null);
    expect(scope.author).toBe(null);
    expect(scope.published).toBe(null);
    expect(scope.media).toBe(null);
    expect(scope.tags).toBe(null);
  });
});
