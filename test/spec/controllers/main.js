'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('flickrPublicFeedApp'));

  var MainCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should have empty feed on start', function () {    
    expect(scope.publicFeed).toBe(null);
  });

  it('should have empty post on start', function () {    
    expect(scope.post).toBe(null);
  });    

});
