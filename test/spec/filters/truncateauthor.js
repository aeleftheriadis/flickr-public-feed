'use strict';

describe('Filter: truncateAuthor', function () {

  // load the filter's module
  beforeEach(module('flickrPublicFeedApp'));

  // initialize a new instance of the filter before each test
  var truncateAuthor;
  beforeEach(inject(function ($filter) {
    truncateAuthor = $filter('truncateAuthor');
  }));

  it('should return the input prefixed with "truncateAuthor filter:"', function () {
    var text = 'email@domain.com (Author)';
    expect(truncateAuthor(text)).toBe('Author');
  });

});
