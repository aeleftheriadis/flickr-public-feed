'use strict';

describe('Filter: truncateTitle', function () {

  // load the filter's module
  beforeEach(module('flickrPublicFeedApp'));

  // initialize a new instance of the filter before each test
  var truncateTitle;
  beforeEach(inject(function ($filter) {
    truncateTitle = $filter('truncateTitle');
  }));

  it('should return the input prefixed with "truncateTitle filter:"', function () {
    var text = 'angularjs';
    expect(truncateTitle(text)).toBe(text);
    var text = 'angularjsangularjsangularjsangularjsangularjsangularjs';
    expect(truncateTitle(text)).toBe('angularjsangularjsangularjsangularjsangu...');
  });

});
