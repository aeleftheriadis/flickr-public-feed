'use strict';

describe('Filter: ordinalDate', function () {

  // load the filter's module
  beforeEach(module('flickrPublicFeedApp'));

  // initialize a new instance of the filter before each test
  var ordinalDate;
  beforeEach(inject(function ($filter) {
    ordinalDate = $filter('ordinalDate');
  }));

  it('check the dates returned by "ordinalDate filter"', function () {
    var text = new Date('01/01/2001');
    expect(ordinalDate(text)).toBe('1st Jan 2001 at 00:00');
    text = new Date('01/02/2001');
    expect(ordinalDate(text)).toBe('2nd Jan 2001 at 00:00');
    text = new Date('01/03/2001');
    expect(ordinalDate(text)).toBe('3rd Jan 2001 at 00:00');
    text = new Date('01/04/2001');
    expect(ordinalDate(text)).toBe('4th Jan 2001 at 00:00');
  });

});
