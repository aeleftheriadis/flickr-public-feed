'use strict';

describe('Filter: truncateLink', function () {

  // load the filter's module
  beforeEach(module('flickrPublicFeedApp'));

  // initialize a new instance of the filter before each test
  var truncateLink;
  beforeEach(inject(function ($filter) {
    truncateLink = $filter('truncateLink');
  }));

  it('should return the input prefixed with "truncateLink filter:"', function () {
    var text = 'test/test/';
    expect(truncateLink(text)).toBe('https://www.flickr.com/photos/test');
  });

});
