# flickr-public-feed

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development
Run bower install && npm install then
run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

##Tasks

I have created the basic tasks on this app and also the additional tasks.

I have created search filter by tag, so the user can search for "potatoes" and filter the results.
Additionally I have added load more icon, and google plus icon for every post.

##App Structure
The app is included in the app folder where controllers, views, directives, filters, and services reside.
I have used Twitter Bootstrap as a CSS Framework.

##Tests
Also there is a test folder, where I have included some basic unit tests.

##Libraries
I have used the directive for the google plus icon from the following repo:
https://github.com/cornflourblue/angulike



