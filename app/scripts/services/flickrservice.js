'use strict';

/**
 * @ngdoc service
 * @name flickrPublicFeedApp.flickrService
 * @description
 * # flickrService
 * Service in the flickrPublicFeedApp.
 */
angular.module('flickrPublicFeedApp')
  .service('flickrService', function ($http) {
    delete $http.defaults.headers.common['X-Requested-With'];
    this.getPublicFeed = function($tag) {
      return $http({
        method:'jsonp',
        url: 'https://api.flickr.com/services/feeds/photos_public.gne',
        params: {
          tags: $tag,
          tagmode: 'all',
          jsoncallback: 'JSON_CALLBACK',
          format: 'json'
        },
        headers: {'Content': 'application/json'}
      });
    };
  });