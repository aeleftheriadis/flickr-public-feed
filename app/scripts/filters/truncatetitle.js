'use strict';

/**
 * @ngdoc filter
 * @name flickrPublicFeedApp.filter:truncateTitle
 * @function
 * @description
 * # truncateTitle
 * Filter in the flickrPublicFeedApp.
 */
angular.module('flickrPublicFeedApp')
  .filter('truncateTitle', function () {
    return function (input) {
      return input.length>40 ? input.substring(40,input) + '...' : input;
    };
  });


