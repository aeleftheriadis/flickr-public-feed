'use strict';

/**
 * @ngdoc filter
 * @name flickrPublicFeedApp.filter:truncateAuthor
 * @function
 * @description
 * # truncateAuthor
 * Filter in the flickrPublicFeedApp.
 */
angular.module('flickrPublicFeedApp')
  .filter('truncateAuthor', function () {
    return function (input) {
      return input.substring(input.lastIndexOf("(")+1,input.lastIndexOf(")"));
    };
  });

