'use strict';

/**
 * @ngdoc filter
 * @name flickrPublicFeedApp.filter:truncateLink
 * @function
 * @description
 * # truncateLink
 * Filter in the flickrPublicFeedApp.
 */
angular.module('flickrPublicFeedApp')
  .filter('truncateLink', function () {
    return function (input) {
      var parts = input.split("/");
      return 'https://www.flickr.com/photos/'+parts[parts.length - 3];
    };
  });
