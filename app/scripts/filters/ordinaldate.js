'use strict';

/**
 * @ngdoc filter
 * @name flickrPublicFeedApp.filter:ordinalDate
 * @function
 * @description
 * # ordinalDate
 * Filter in the flickrPublicFeedApp.
 */
angular.module('flickrPublicFeedApp')
  .filter('ordinalDate', function ($filter) {
    var suffixes = ["th", "st", "nd", "rd"];
    return function(input) {
      var dtfilter = $filter('date')(input, 'd');
      var restDate = $filter('date')(input, 'MMM yyyy');
      var time =  $filter('date')(input, 'HH:mm');
      var day = parseInt(dtfilter.slice(-1));
      var relevantDigits = (day < 30) ? day % 20 : day % 30;
      var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
      return dtfilter+suffix + ' ' + restDate + ' at '+ time;
    };
  });
