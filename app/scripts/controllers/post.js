'use strict';

/**
 * @ngdoc function
 * @name flickrPublicFeedApp.controller:PostCtrl
 * @description
 * # PostCtrl
 * Controller of the flickrPublicFeedApp
 */
angular.module('flickrPublicFeedApp')
  .controller('PostCtrl', function ($scope,$rootScope,$location) {
    $scope.title = null;
    $scope.link = null;
    $scope.author = null;
    $scope.published = null;
    $scope.media = null;
    $scope.tags = null;
    $scope.goToHome = function () {
      $location.path( '/' );
    };

    if( typeof $rootScope.post === 'undefined'){
      $location.path( '/' );
    }
    else{
      $scope.title = $rootScope.post.title;
      $scope.link = $rootScope.post.link;
      $scope.author = $rootScope.post.author;
      $scope.published = $rootScope.post.published;
      $scope.media = $rootScope.post.media.m;
      $scope.tags = $rootScope.post.tags.split (' ');
    }

  });





