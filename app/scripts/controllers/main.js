'use strict';

/**
 * @ngdoc function
 * @name flickrPublicFeedApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the flickrPublicFeedApp
 *
 * ['myApp.directives', 'myApp.services']
 */
angular.module('flickrPublicFeedApp')
  .controller('MainCtrl', function ($scope,$rootScope,$location,flickrService) {
    $rootScope.post = null;
    $scope.publicFeed = null;
    flickrService.getPublicFeed('potato').then(function(response) {
      $scope.publicFeed = response.data.items;
    });

    var pagesShown = 1;
    var pageSize = 5;

    $scope.itemsLimit = function() {
      return pageSize * pagesShown;
    };
    $scope.hasMoreItemsToShow = function() {
      return $scope.publicFeed !==null ? pagesShown < ($scope.publicFeed.length / pageSize) : true;
    };
    $scope.showMoreItems = function() {
      pagesShown = pagesShown + 1;
    };

    $scope.showPost = function ( post ) {
      $rootScope.post = post;
      $location.path( '/post' );
    };

    $scope.showAll = function(){
      if($scope.search.tags === ''){
        pageSize = 5;
      }
      else{
        pageSize = 20;
      }
    };
    
  });

